// console.log("Hello");


let x = 1234;
let y = 12345;

let sum = x + y;
console.log("Result: " + sum);

let diff = x - y;
console.log(diff);

let q = x / y;
console.log("Division: " + q);

let remainder = y % x;
console.log(remainder);

// ASIGNMENT OPERATOR '='
let assi = 8;
assi = assi + 2;
console.log("Result: " + assi);

//shorthand method for assi
assi += 2;

assi -= 2;
assi *= 2;
assi /= 2;

//multiple operators and parenthesis
let mdas = 1 + 2 + 3 * 4 / 5;
console.log("result " + mdas);

let pemdas = 1 + (2-3) * (4/5);
console.log(pemdas);

pemdas = (1 + (2-3)) * (4/5);

//incrementation vs decrementation
//increment (++)
// decrementation (--)

// ++z pre-increment
// z++ post-increment
let z = 1;
let incre = ++z;
console.log(incre);

let decrement = --z;
console.log(decrement);
console.log(z);

decrement = z--;
console.log(decrement);

//type coercion
let numA = "10";
let numB = 12;

let co = numA + numB;
console.log(co);
console.log(typeof co);

let numC = 16;
let numD = 14;

let nonCo = numC + numD;
console.log(nonCo);
console.log(typeof nonCo);

let numE = false + 1;
console.log(numE);

numE = true + 1;
console.log(numE);

//comparison Operators

let juan = "juan";

// Equality Operator (==)
// Checks 2 operands if they're the same value
//  = assignment
// == compare but excluded the data type
// === strict equality, compare content and data type
console.log(1==1); 

// inequality operator (!=)
// Strict inequality (!==)

console.log(1 != "1");

//relational operator < > >= <=
let a = 50;
let b = 51;

console.log(a < b);

let numStr = "30";
console.log(a > numStr);

//NaN = Not A Number
// AND operator (&&)
// OR operator (||)
// NOT operator (!)